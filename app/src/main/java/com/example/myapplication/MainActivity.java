package com.example.myapplication;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    private TextView textView;
    private TextView textView2;
    private LocationManager locmana;

    private LocationListener locationListener = new LocationListener() {
        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void onLocationChanged(Location location) {
            updateText2();
        }

        @Override
        public void onProviderDisabled(String s) {
            updateText2("provider disabled");
        }

        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void onProviderEnabled(String s) {
            updateText2();
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }
    };

    private void updateText2(String str) {
        textView2.setText(str);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void updateText2() {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling

                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},42 );
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
                updateText2("didn't got permissions");
                return;
            }
        Location gpsLoca = locmana.isProviderEnabled(locmana.GPS_PROVIDER) ? locmana.getLastKnownLocation(locmana.GPS_PROVIDER) : null;
        Location netLoca = locmana.isProviderEnabled(locmana.GPS_PROVIDER) ? locmana.getLastKnownLocation(locmana.NETWORK_PROVIDER): null;
        StringBuilder str= new StringBuilder();
        if(gpsLoca != null)str.append(Html.fromHtml(String.format("Latitude:<br />\n" +
                        "<font color=\"#FF5722\"><big><big><big>%s</big></big></big></font><br /><br />\n" +
                        "Longitude:<br /><font color=\"#FF5722\"><big><big><big>%s</big></big></big></font><br /><br />\t\tAccuracy:<br />\n" +
                        "<font color=\"#FF5722\"><big><big><big>%f</big></big></big></font><br /><br />",
                gpsLoca.getLatitude(), gpsLoca.getLongitude(),gpsLoca.getAccuracy())));
        if(netLoca != null)str.append(Html.fromHtml(String.format("Latitude:<br /><font color=\"#FF5722\"><big><big><big>%s</big></big></big></font><br /><br />\n" +
                        "Longitude:<br /><font color=\"#FF5722\"><big><big><big>%s</big></big></big></font><br /><br />\t\tAccuracy:<br />\n<font color=\"#FF5722\"><big><big><big>%f</big></big></big></font><br /><br />",
                netLoca.getLatitude(), netLoca.getLongitude(),netLoca.getAccuracy())));
        updateText2(str.toString());
    }

    private Thread updateThread = new Thread(){
        public void run(){
            try{
                while (true){
                    MainActivity.this.runOnUiThread(new Runnable() {
                        @TargetApi(Build.VERSION_CODES.M)
                        @Override
                        public void run() {
                            updateText();
                            updateText2();
                        }
                    });
                    Thread.sleep(1000);
                }
            }
            catch (InterruptedException e){
                //thread fini => sort de loop
            }
        }
    };//fin de declaration du thread
    int sec=0;

    private void updateText() {
        textView.setText(Integer.toString(sec));
        sec++;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        locmana = (LocationManager) getSystemService(LOCATION_SERVICE);
        
        textView = findViewById(R.id.t);
        textView2 = findViewById(R.id.t2);
        textView2.setMovementMethod(new ScrollingMovementMethod());
        updateThread.start();
    }
}
